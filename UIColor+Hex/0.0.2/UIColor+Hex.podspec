Pod::Spec.new do |spec|

    spec.name              	= 'UIColor+Hex'
    spec.version           	= '0.0.2'
    spec.summary           	= 'initial UIColor with hex string'
    spec.homepage          	= 'https://bitbucket.org/bazaku/uicolor-hex/overview'
    spec.license      		= 'MIT'
    spec.author            	= {
        'bazaku' => 'bazaku@gmail.com'
    }
    spec.source            	= {
        :git => 'https://bazaku@bitbucket.org/bazaku/uicolor-hex.git',
        :tag => '0.0.2'
    }
    spec.source_files      	= 'Classes/*.{m,h}'
    spec.requires_arc      	= true
	spec.frameworks   		= 'Foundation', 'UIKit'
	spec.platform			= :ios, '5.0'

end