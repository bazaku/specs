Pod::Spec.new do |spec|

    spec.name              	= 'MasterAppApi'
    spec.version           	= '0.9.7'
    spec.summary           	= 'MasterApp API'
    spec.homepage          	= 'https://scm.onewebuxp.allianz/scm/git/core/mobile/masterapp/api'
    spec.license      		= 'MIT'
    spec.author            	= {
        'bazaku' => 'bazaku@gmail.com'
    }
    spec.source            	= {
        :git => 'https://scm.onewebuxp.allianz/scm/git/core/mobile/masterapp/api',
        :tag => 'v.0.9.7'
    }
    spec.source_files      	= '**/*.{h,m}'
	spec.exclude_files	 	= '_DEPRICATED/**/*', '_STILLBETA/**/*'
	spec.resources    		= 'MAAPI.xcassets/**/*.png'
    spec.requires_arc      	= true
	spec.platform			= :ios, '5.0'

end